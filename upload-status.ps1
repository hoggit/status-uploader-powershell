param($StatusKey, $JsonFile)
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
while($true){
        try {
                $data = Get-Content -Raw -Path $JsonFile | ConvertFrom-Json

                $params = @{
                key= $StatusKey
                status = @{
                        map= "map"
                        missionName= "missionName"
                        players= 0
                        maxPlayers = 0
                        serverName = "SAWThingy"
                        data = $data
                }
                } | ConvertTo-Json -Depth 10

                #Write-Output $params
                Invoke-WebRequest -UseBasicParsing -ContentType "application/json" -Uri https://status.hoggitworld.com/$StatusKey -Method POST -Body $params
        }
        catch {
                Write-Host "An Error occured: "
                Write-Host $_
        }
        finally {
                Write-Host "Sleeping for 30 sec"
                Start-Sleep -s 30
        }
}